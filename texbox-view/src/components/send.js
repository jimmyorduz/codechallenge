import React from 'react'
import './send.css'

const send = () => {
    return (

    <nav className="navbar">
        <form className="form-inline">
          <input className="form-control mr-sm-2" type="text" placeholder="Insert Text" aria-label="text"/>
          <button className="btn btn-primary my-2 my-sm-0" type="submit">Send</button>
        </form>
    </nav>

    )
}

export default send

 