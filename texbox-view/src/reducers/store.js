import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import textReduce from './text-reduce';

const mainReducer = combineReducers({texts: textReduce});

export default createStore(mainReducer,applyMiddleware(thunk));