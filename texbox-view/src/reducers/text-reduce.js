
const initialState = {
    body:{isPalindrome: false,
    text: '',
    error: null}
};

const FETCH_TEXT_SUCCESS = 'FETCH_TEXT_SUCCESS';
const FETCH_TEXT_ERROR = 'FETCH_TEXT_ERROR';
const getTextSuccess = (body) => ({ type: FETCH_TEXT_SUCCESS, body: body });
const getTextError = (body) => ({ type: FETCH_TEXT_SUCCESS, body: body });

export const fetchText=(text ='ama') =>{
    return dispatch => {
        let itext=text;
        fetch(`http://localhost:8080/iecho?text=${encodeURIComponent(itext)}`)
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(getTextSuccess(res.body));
            console.log("Body: " + res.body);
            return res.body;
        })
        .catch(error => {
            dispatch(getTextError(error));
        })
    }
}

export default (state =initialState, action) => {
    console.log("valor de action: "+action.type);
    switch(action.type) {
        case FETCH_TEXT_SUCCESS:
            return {
                ...state,
                body : { 
                    isPalindrome: action.body.isPalindrome,
                    text: action.body.text,
                    error:''
                }
            }
        case FETCH_TEXT_ERROR:
            return {
                ...state,
                body : { 
                    isPalindrome: false,
                    text:'',
                    error: action.body.error
                }
            }
        default: 
            return {...state};
    }
}