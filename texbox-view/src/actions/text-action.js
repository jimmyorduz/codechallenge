export const FETCH_TEXT_SUCCESS = 'FETCH_TEXT_SUCCESS';
export const FETCH_TEXT_ERROR = 'FETCH_TEXT_ERROR';

export function fetchTextSuccess() {
    return {
        type: FETCH_TEXT_SUCCESS
    }
}

export function fetchTextError() {
    return {
        type: FETCH_TEXT_ERROR
    }
}
