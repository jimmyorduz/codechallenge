'use strict'
import { expect } from 'chai'
import { app } from './app.js'
import request from 'supertest'

describe('GET /iecho', function () {
  it('successful test that return the a word invert where is palindrome', async function () {
    const res = await request(app).get('/iecho').query({ text: 'ama' })
    expect(res.status).to.equals(200)
    expect(res.body.text).to.equals('ama')
    expect(res.body.isPalindrome).to.equals(true)
  })
})

describe('GET /iecho', function () {
  it('successful test that return the a word invert where not is palindrome', async function () {
    const res = await request(app).get('/iecho').query({ text: 'test' })
    expect(res.status).to.equals(200)
    expect(res.body.text).to.equals('tset')
    expect(res.body.isPalindrome).to.equals(false)
  })
})

describe('GET /iecho', function () {
  it('fail test that return exception', async function () {
    const res = await request(app).get('/iecho').query({ text: '' })
    expect(res.status).to.equals(400)
    expect(res.body.error).to.equals('No Text')
  })
})
