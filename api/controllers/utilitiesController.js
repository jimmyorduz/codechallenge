'use strict'
import { TextToolBox } from '../models/textToolBox.js'
import { ResponseToolBox } from '../models/responseToolBox.js'
import { FunctionalException } from '../models/functionalException.js'

function evaluateText (text) {
  if (text.length > 0) {
    const textT = new TextToolBox(text)
    const requestT = new ResponseToolBox(textT.text, textT.invertText(), textT.isPalindrome(), '200', '')
    return requestT
  } else {
    throw new FunctionalException('userException', 'No Text', 1001)
  }
}
export { evaluateText }
