'use strict'

class ResponseToolBox {
  constructor (text, invertText, isPalindrome, status, msj) {
    this.text = text
    this.invertText = invertText
    this.isPalindrome = isPalindrome
    this.status = status
    this.msj = msj
  }
}
export { ResponseToolBox }
