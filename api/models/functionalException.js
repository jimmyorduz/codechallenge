'use strict'
class FunctionalException {
  constructor (name, msj, code) {
    this.name = name
    this.msj = msj
    this.code = code
  }
}
export { FunctionalException }
