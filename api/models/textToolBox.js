'use strict'

class TextToolBox {
  constructor (text) {
    this.text = text
  }

  invertText () {
    let newText = ''
    const arrayText = this.text.split('')
    const reversedtext = arrayText.reverse()
    for (let i = 0; i < reversedtext.length; i++) {
      newText += reversedtext[i]
    }
    return newText
  }

  isPalindrome () {
    const newText = this.text.toLowerCase().replace('', ' ')
    const invertTextVar = this.invertText().toLowerCase().replace('', ' ')
    if (newText === invertTextVar) {
      return true
    } else {
      return false
    }
  }
}

export { TextToolBox }
