'use strict'
import express from 'express'
import { evaluateText } from './controllers/utilitiesController.js'
const app = express()

// cargar Rutas

app.get('/iecho', (req, res) => {
  const text = req.query.text
  try {
    const responseToolBox = evaluateText(text)
    res.status(responseToolBox.status).send({ text: responseToolBox.invertText, isPalindrome: responseToolBox.isPalindrome })
  } catch (e) {
    res.status(400).send({ error: e.msj })
  }
})
export { app }
