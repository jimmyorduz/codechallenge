# Proyecto api 

Proyecto donde esta toda la lógica Backend desarrollada con las tecnologias exigidas:

NodeJs https://nodejs.org/en/
ExpressJs https://expressjs.com/
Mocha https://mochajs.org/
Chai https://www.chaijs.com/
SuperTest https://github.com/visionmedia/supertest#readme
https://standardjs.com/

sumando los dos puntos opcionales:

1. En la respuesta, indicar por medio del flag "palindrome": true si el texto enviado es un palindromo https://es.wikipedia.org/wiki/Pal%C3% ADndromo
2. Usar StandarJs https://standardjs.com/

## instalación 

1. Instalar dependencias
    'npm install'
2. Ejecutar pruebas
    'npm test'
3. Ejecutar el api localmente
    'npm start'

# Proyecto Frontend

Proyecto donde esta el frontend 

NodeJs https://nodejs.org/en/ 
Webpack https://webpack.js.org/ 
Bootstrap https://getbootstrap.com/ 
React https://reactjs.org/

Se alcanza a desarrollar el wireframe, pero no se finaliza la comunicación entre en back con el front.